## Getting Started (dev mode)

First, install the dependencies: 
```bash
npm install
```

Then run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the react app working.
The api should be up on [http://localhost:8080](http://localhost:8080)

## Build for production

On `production` mode the app runs on a single server and all the front-end related files are statically served by the express
```bash
npm run start
```
The app is going to be located at [http://localhost:8080](http://localhost:8080)

### Notes on the API

The current API only supports the following endpoints:

```bash 
GET /api/ Returns the current ammount on the sole account we are managing 
GET /api/transactions Lists all the recorded transactions
POST /api/transactions Adds a new transaction, expects a json payload with a shape like {"type": [credit|debit], "amount": integer}

GET /api/transaction/[id] Returns the details of a particular transaction if available.
```