import React from 'react';
import moment from 'moment';

import './styles.css';

const Transaction = ({transaction, extended = false, onClick}) => {
  const {id, type, amount, effectiveDate} = transaction;
  return (
    <div className={`transaction-item ${type}`} onClick={onClick}>
      <div className="summary">
        <div className="type"><span>{type}</span></div>
        <div className="amount"><span>$</span>{amount}</div>
      </div>
      
      {extended && 
        <div className="extended">
          <p>Id: {id} </p>
          <p>Date: {moment(effectiveDate).format("lll")}</p>
        </div>
      }
    </div>
  )
}

export default Transaction;