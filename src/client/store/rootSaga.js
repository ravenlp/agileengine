
import { all, fork} from 'redux-saga/effects';
import TransactionsSagas from './transactions/saga';


export default function* rootSaga() {
  yield all([
    ...TransactionsSagas
  ]);
}
