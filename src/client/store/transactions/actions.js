import {
  FETCH_TRANSACTIONS_REQUEST,
  FETCH_TRANSACTIONS_SUCCESS,
  FETCH_TRANSACTIONS_FAILURE
} from './types';

export const fetchTransactions = () => ({
  type: FETCH_TRANSACTIONS_REQUEST
})

export const fetchTransactionsSuccess = (payload) => ({
  type: FETCH_TRANSACTIONS_SUCCESS,
  payload
})

export const fetchTransactionsFail = () => ({
  type: FETCH_TRANSACTIONS_FAILURE
})
