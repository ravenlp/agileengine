import {takeLatest, call, put} from 'redux-saga/effects';
import apiCaller from '../../utils/apiCaller';

import { fetchTransactionsSuccess, fetchTransactionsFail } from './actions';
import { FETCH_TRANSACTIONS_REQUEST } from './types';

const postApiUrl = 'api/'
console.log()

function* fetchTransactions() {
  try{
    const res = yield call(apiCaller, postApiUrl, 'GET', 'transactions');
    yield put(fetchTransactionsSuccess(res))
  } catch (err) {
    yield put(fetchTransactionsFail())
    console.error(err)
  }
}

export default [
  takeLatest(FETCH_TRANSACTIONS_REQUEST, fetchTransactions)
]