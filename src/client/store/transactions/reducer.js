import {
  FETCH_TRANSACTIONS_REQUEST,
  FETCH_TRANSACTIONS_SUCCESS,
  FETCH_TRANSACTIONS_FAILURE
} from './types';

const initialState = {
  historic: [],
  loading: false
}

const loaderReducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_TRANSACTIONS_REQUEST: 
      return {
        ...state,
        loading: true
      }
    case FETCH_TRANSACTIONS_SUCCESS: 
      return {
        ...state,
        loading: false,
        historic: action.payload
      }
    case FETCH_TRANSACTIONS_FAILURE: 
      return {
        ...state,
        loading: false
      }
    default:
      return state;
  }
}

export default loaderReducer;