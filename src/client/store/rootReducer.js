import {combineReducers} from 'redux';
import transactionReducer from './transactions/reducer'

export default combineReducers({
  historic: transactionReducer,
});