import React, {useState, useEffect, useCallback} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import Transaction from './components/transactions';
import {fetchTransactions} from './store/transactions/actions'
import {getAllTransactionsSelector, getLoadingState} from './store/transactions/selectors'


const App = () => {

  const dispatch = useDispatch();

  const dispatchToProps = {
    fetchTransactionsCall: useCallback(() => dispatch(fetchTransactions()), [dispatch])
  }

  useEffect(() => {
    dispatchToProps.fetchTransactionsCall();
  }, []);

  let transactions = [];
  transactions = useSelector((state) => getAllTransactionsSelector(state));
  const loading = useSelector((state) => getLoadingState(state));
  const [selected, setSelected] = useState(0);

  return (
    <div className="container">
        <header>
          <h1>Transaction History {loading === true && `(Loading)`}</h1>
        </header>
        <div className="listing">
          {transactions.map(t => (
            <Transaction
              transaction={t}
              extended= {selected === t.id}
              key={t.id}
              onClick={() => {
                if(selected === t.id){
                  setSelected(0);
                } else {
                  setSelected(t.id);
                }
              }}
            />
          ))}
          {transactions.length === 0 && !loading && (
            <h2>No transactions so far</h2>
          )}
        </div>
    </div>
  )
}

export default App;