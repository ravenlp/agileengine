
import express from 'express';
import bodyParser from 'body-parser';
import transactionsRouteHandler from './controllers/transactions';
import singleTransactionRouterHandler from './controllers/transaction'
import accountBalanceHandler from './controllers/account';

const app = express();
app.use(bodyParser());

app.use(express.static('dist'));

app.get('/api/getUsername', (req, res) => res.send({ username: 'sandunga' }));

app.get('/api/', (req, res) => {
  accountBalanceHandler.get(res);
});

app.get('/api/transactions', (req, res) => {
  transactionsRouteHandler.get(res);
})

app.post('/api/transactions', (req, res) => {
  const body = req.body;
  transactionsRouteHandler.post(body, res);
})

app.get('/api/transaction/:id', (req, res) => {
  singleTransactionRouterHandler.get(req, res)
})

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
