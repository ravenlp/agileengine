import historicLog from '../utils/storage';
import { sendJson, sendError } from '../utils/responses';

const handleRequest = async (req, res) => {
  const {
    params: { id }
  } = req
  if(!id) {
    sendError(400, 'Invalid transaction id');
    return
  }

  const transaction = await historicLog.get(id);
  if(transaction && transaction.id) {
    sendJson(200, transaction, res);
  } else {
    sendError(404, `Transaction ${id} not found`, res);
  }
} 

export default {
  get: handleRequest
}