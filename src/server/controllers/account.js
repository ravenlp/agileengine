import { sendJson } from '../utils/responses';
import userAccount from '../models/account';

export default {
  get: (res) => sendJson(200, {amount: userAccount.amount}, res)
}