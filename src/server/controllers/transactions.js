import historicLog from '../utils/storage';
import { sendJson, sendError  } from '../utils/responses';
import userAccount from '../models/account';
import { transactionValidator } from '../utils/validators';

const handleNewTransaction = async (transaction, res) => {
  try {
    // Check if the transaction has the expected shape
    transactionValidator(transaction);

    const {type, amount} = transaction;
    switch(type) {
      case 'debit': 
        const storedDebit = await userAccount.debit(amount);
        sendJson(201, storedDebit, res);
        break;
      case 'credit':
        const storedCredit = await userAccount.credit(amount);
        sendJson(201, storedCredit, res);
        break;
      default: 
        throw new Error('Invalid transaction type');
    }
  } catch (e) {
    sendError(400, e.message, res)
  }
}

const handleTransactionHistory = async (res) => {
  const allRecords = await historicLog.all();
  sendJson(200, allRecords, res)
}

export default {
  post: (body, res) => handleNewTransaction(body, res),
  get: (res) => handleTransactionHistory(res)
}