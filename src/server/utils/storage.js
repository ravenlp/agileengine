import locks from 'locks';

/**
 * This is a simple in-memory storage implementation. 
 * It will get emptied every time the server restarts
 * 
 * It has a stardard readers-writers locking policy 
 * https://en.wikipedia.org/wiki/Readers%E2%80%93writer_lock
 * 
 */

const rwlock = locks.createReadWriteLock();

let storage = [];
let nextId = 1;


export default {
  all: () => {
    const p = new Promise((resolve, reject) => {
      rwlock.readLock(() => {
        resolve(storage);
        rwlock.unlock();
      });
    })
    return p;
  },
  
  get: (id) => {
    const p = new Promise((resolve, reject) => {
      rwlock.readLock(() => {
        const itemId = parseInt(id);
        if(itemId) {
          const find = storage.find((t) => {
            return t.id === itemId;
          })
          if(!find){
            resolve({})
          }
          resolve(find);
        }
        rwlock.unlock();
      });
    });
    return p;
  },
  
  set: (transaction) => {
    const p = new Promise((resolve, reject)=> {
      
      // Lock the storage when writing
      rwlock.writeLock(() => {
        const newItem = {
          ...transaction,
          id: nextId,
          effectiveDate: new Date().toISOString()
        }
        storage.push(newItem);
        nextId = nextId + 1;

        // Unlocks the resource for other to take it
        rwlock.unlock(); 
        resolve(newItem);
      });
    });
    
    return p
  }
}