const validTypes = ['credit', 'debit'];

const transactionValidator = (transaction = {}) => {
  if(!transaction.type || !validTypes.includes(transaction.type)){
    throw new Error(`Invalid transaction type, must be one of [${validTypes.join('|')}]`);
  }

  if(!transaction.amount || parseInt(transaction.amount) < 0 ) {
    throw new Error('Invalid amount');
  }
}

export {
  transactionValidator
}