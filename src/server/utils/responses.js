// Some response related helper functions

const sendJson = (code = 200, payload = {}, res) => {
  res.append('Content-Type', 'application/json')
  res.status(code).end(JSON.stringify(payload))
};

const sendError = (code = 400, message = '', res) => {
  res.status(code).end(message)
};

const methodNotAllowed = (methods, res) => {
  res.append('Allow', methods)
  res.status(405).end(`Method ${method} Not Allowed`);
}

export {
  sendJson,
  sendError,
  methodNotAllowed
}