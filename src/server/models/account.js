import historicLog from '../utils/storage';

class Account {
  constructor() {
    this._amount = 0;
  }

  get amount() {
    return this._amount;
  }

  set amount(amount) {
    this._amount = amount;
  }

  async debit(amount) {
    if(amount > this._amount) {
      throw new Error("Invalid debit, insufficient founds");
    }
    this._amount = this._amount - amount;
    const log = await historicLog.set({amount, type: 'debit'});
    return log;
    
  }

  async credit(amount) {
    this._amount = this._amount + amount;
    const log = await historicLog.set({amount, type: 'credit'});
    return log;
  }
}

export default new Account();